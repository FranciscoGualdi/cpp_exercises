/*

Lets say you have array A[0]=1 A[1]=-1 ....A[n]=x

Then what would be the smartest way to find out the number of times when A[i]+A[j] is even where i < j

So if we have {1,2,3,4,5} we have 1+3 1+5 2+4 3+5 = 4 pairs which are eve

*/

#include <iostream>
#include <cstdlib>


int count_sum_of_even_pairs(int arr[], int size)
{
    if (!arr || size < 2) {
      return 0;
    }

    int evenNumbersCount = 0;
    int oddNumberCount = 0;

    for(int i = 0; i < size; i++) {
      int v = arr[i];
      if (v % 2 == 0) {
        evenNumbersCount++;
      } else {
        oddNumberCount++;
      }
    }

    int ret = (evenNumbersCount * (evenNumbersCount - 1)) / 2 + (oddNumberCount * (oddNumberCount - 1)) / 2;
    return ret > 1000000000 ? -1 : ret;
}

int main()
{
    int arr[]={1, 2,3,4,5}; // res = 4

    int res = count_sum_of_even_pairs(arr,5);

    std::cout << res << std::endl;

    return 0;
}
