#include <iostream>
#include <unordered_map>
#include <map>
#include <stdio.h>
#include <string.h>
#include <string>
#include <algorithm>
#include <list>
#include <fstream>

//arguments
int main(int argc/*number of args*/, char **argv/*args vector*/)
{
    //    argc would be 3.
    //    argv[0] would be "./program". // name of program
    //    argv[1] would be "hello".     // effective the args
    //    argv[2] would be "world".


    // input file
    std::ifstream iFile("input.txt");    // input.txt has integers, one per line
    while (true) {
        int x;
        iFile >> x;
        if( iFile.eof() ) break;
        std::cerr << x << std::endl;
    }


    //hackerrank input

    int n;
    std::cin >> n;
    std::vector<int> a(n);
    for(int a_i = 0; a_i < n; a_i++){
        std::cin >> a[a_i];
    }

}
