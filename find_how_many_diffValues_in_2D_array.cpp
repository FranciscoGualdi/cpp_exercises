/*
 *You are given an image of a surface photographed by a satellite.
 * The image is a bitmap where water is marked by '.' and land is marked by '*'.
 * Adjacent group of '*'s form an island.
 * (Two '*' are adjacent if they are horizontal, vertical or diagonal neighbours).
 * Your task is to print the number of islands in the bitmap
 */

 
#include <stdio.h>
#include <map>
#define COLS 12 // last /0

void diffsElements(char map[][COLS],  int rowCount, std::map<char, int> &diffs)
{

    int i, j;
    for(i=0; i<rowCount; ++i){
        for(j=0; j<COLS; ++j){
            char value = map[i][j];
            if(value == '\0') continue; // \0 is not a valid character
            diffs[value] = diffs[value] + 1;
        }
    }
}

int main()
{
    char map[][COLS] = {
                    "***.......*",
                    "*.........*",
                    "**.........",
                    "**.........",
                    "*.........*",
                    "*.........*"
                    };
    int rows = sizeof(map)/sizeof(map[0]);

    std::map<char, int> diffs;
    diffsElements(map, rows, diffs);


    printf("No. of diffs = %d\n", diffs.size());


    for(auto i : diffs){
        printf("diffs = %c\n", i);

    }

    return 0;
}
