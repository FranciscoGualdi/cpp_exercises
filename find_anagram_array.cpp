#include <iostream>
#include <unordered_map>
#include <map>
#include <string>
#include <algorithm>
#include <vector>

using namespace std;

typedef std::unordered_map<std::string,std::vector<std::string> > MM;

template <typename ITER>
MM find_anagrams(ITER b, ITER e) {
    MM ret;
    for ( ; b != e; ++b) {
        std::string key{*b};
        std::sort(key.begin(),key.end()); //the ordered word is the key
        ret[key].push_back(*b);
    }
    return ret;
}

int main()
{
    // mock values
    std::vector<std::string> x{"trees", "bike", "cars", "steer", "arcs"} ;

    auto y = find_anagrams(x.begin(),x.end());

    for (auto& v : y) {
        std::cout << "Bucket " << v.first << std::endl;
        for (auto& s : v.second) {
            std::cout << " " << s << std::endl;
        }
    }
    return 0;
}
