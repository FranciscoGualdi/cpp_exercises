#include <iostream>
#include <vector>
#include <string>

/*
Write a program which accepts a variable number of strings as input and locates the position of the character 'a' and the
string 'is' in each input. The program should print the sum of the positions found. If a particular string or character is not found
then the position should be considered to be -1.
For example, if the word 'mistake' is supplied to the program, the string 'is' lies at position 1 (considering the first position to
be zero) and the character 'a' lies at position 4, hence the sum of the positions will be 4+1=5. Similarly if the word "test" is
supplied to the program, both 'a' and 'is' are not present, hence the result will be (-1) + (-1) = -2.
Below is an example of the input supplied to the program followed by the output. The first line of input indicates the number of
strings supplied to the program, followed by the actual strings (one per line)

  Example:
  Input
  
  3 (indicating that 3 string will be supplied to the program)
  it is a simple problem
  raspberry is sweet
  test
  
  OUTPUT
  
  9
  11
  -2
*/

int sum(std::string str, char *key ) {
    size_t nPos = str.find(key, 0); // fist occurrence
    int sum = nPos;
    while(nPos != std::string::npos)
    {
        nPos = str.find(key, nPos+1);
        if(nPos != std::string::npos)
            sum = sum + nPos;
    }
    return sum;
}



#define MAXSIZE 10
int main()
{
    std::cout << "Init test_`3" << std::endl;

    //input
    std::string temp;
    int size = 0;
    std::vector<std::string> words;

    std::cout << "Numbers of string: " << std::endl;
    char str[256];
    std::cin.getline(str,256); //size
    size = atoi(str);


    for (int var = 0; var < size && var < MAXSIZE ; ++var) {
        std::cout << "Enter a string: " << std::endl;
        char str[256];
        std::cin.getline(str,256);
        words.push_back(str);
    }

    for (int var = 0; var < size && var < MAXSIZE ; ++var) {
        std::string str = words[var];
        long res_a = sum(str,"a");
        long res_is = sum(str,"is");
        std::cout << "Sum of position 'a' and 'is': " << (res_a+res_is) << std::endl;
    }

    std::cout << "Exit test_3" << std::endl;
}
