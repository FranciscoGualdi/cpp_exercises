
#include <stdio.h>
#include <string.h>
#include <map>
#include <vector>
#include <stdbool.h>
#include <math.h>
#include <unordered_map>

#define M_PI acos(-1.0)

// intrsection between tw circles

double areaOfIntersection(double x0, double y0,double  r0, double x1, double y1, double r1)
{
    double rr0 = r0 * r0;
    double rr1 = r1 * r1;
    double d = sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0));

    // overlap
    if (d > r1 + r0)
    {
        return 0;
    }

    // inside 0
    else if (d <= abs(r0 - r1) && r0 >= r1)
    {
        return M_PI * rr1;
    }

    // inside 1
    else if (d <= abs(r0 - r1) && r0 < r1)
    {
        return M_PI * rr0;
    }

    else
    {
        double phi = (acos((rr0 + (d * d) - rr1) / (2 * r0 * d))) * 2;
        double theta = (acos((rr1 + (d * d) - rr0) / (2 * r1 * d))) * 2;
        double area1 = 0.5 * theta * rr1 - 0.5 * rr1 * sin(theta);
        double area2 = 0.5 * phi * rr0 - 0.5 * rr0 * sin(phi);
        return area1 + area2;
    }
}

double truncate(double x, unsigned int digits) {
    double temp =0.0;

    temp = (int) (x * pow(10,digits));

    temp /= pow(10,digits);

    return temp;
}

//--------------------------------
int main()
{
    double res = areaOfIntersection(2, 2,3, 5, 5, 3);
    res = truncate(res,4);
    return 0;

}

