/*
    revert the string
*/

#include <stdio.h>
#include <string.h>
#include <iostream>
#include <cstdlib>

void revert_string(char in_str[], char out_str[], int size)
{
    int c, d;

    for (c = size - 1, d = 0; c >= 0; c--, d++)
       out_str[d] = in_str[c];

    out_str[d] = '\0';
}

int main()
{

    char str[] = {"devs are nice guys"};
    char out_str[100];

    int size = strlen(str);

    revert_string(str, out_str, size);

    std::cout << "original: " << str << std::endl;
    std::cout << "reverse: " << out_str << std::endl;


    return 0;
}

