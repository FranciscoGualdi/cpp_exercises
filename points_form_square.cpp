#include <iostream>
#include <string>
#include <algorithm>
#include <vector>
#include <math.h>

using namespace std;

#define SQUARE 1
#define RECTANGLE 2
#define OTHER 3

struct Point
{
    int x, y;
};

// tests if angle abc is a right angle
int IsOrthogonal(Point a, Point b, Point c)
{
    return (b.x - a.x) * (b.x - c.x) + (b.y - a.y) * (b.y - c.y) == 0;
}



//-----------------------------------------------
#define sqr(x) ((x)*(x))
int isSquare(double x1, double y1, // or rectangle
                 double x2, double y2,
                 double x3, double y3,
                 double x4, double y4)
{
    double cx,cy;
    double dd1,dd2,dd3,dd4;
    int type = 0;
    cx=(x1+x2+x3+x4)/4;
    cy=(y1+y2+y3+y4)/4;

    dd1=sqr(cx-x1)+sqr(cy-y1);
    dd2=sqr(cx-x2)+sqr(cy-y2);
    dd3=sqr(cx-x3)+sqr(cy-y3);
    dd4=sqr(cx-x4)+sqr(cy-y4);
    bool eq = (dd1==dd2 && dd1==dd3 && dd1==dd4);

    if(cx == cy && eq) // square
        type = SQUARE;
    else if(eq)
        type = RECTANGLE;      // rectangle
    else
        type = OTHER;      // polygon

    return type;
}

int main()
{
//    //case 1 square
//    Point p1 = {20, 10}, p2 = {10, 20},
//            p3 = {20, 20}, p4 = {10, 10};

//    cout << "testing case 1 (should be square):" << isSquare(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y) << endl;

//    //case 2 rectangle
//    p1 = {3, 1}, p2 = {-3, 1},
//    p3 = {3, -2}, p4 = {-3, -2};

//    cout << "testing case 2 (should be rectangle):" << isSquare(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y) << endl;

//    //case 3 rectangle
//    p1 = {0, 0}, p2 = {4, 6},
//    p3 = {0, 6}, p4 = {4, 0};

//    cout << "testing case 3 (should be rectangle):" << isSquare(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y) << endl;

//    //case 4 polygon
//    p1 = {0, 0}, p2 = {4, 7},
//    p3 = {0, 6}, p4 = {4, 0};

//    cout << "testing case 4 (should be polygon):" << isSquare(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y, p4.x, p4.y) << endl;

    Point p1 = {20, 10}, p2 = {10, 20},
          p3 = {20, 20}, p4 = {10, 10};

    int n;
      cin >> n;
      for(int i = 0; i < n; i++) {
          int a, b;
          cin >> a >> b;
          cout << a+b << "\n";
      }

    return 0;
}
