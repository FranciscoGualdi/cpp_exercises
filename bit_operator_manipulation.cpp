// Program to count islands in boolean 2D matrix
#include <stdio.h>
#include <string.h>
#include <map>
#include <vector>
#include <numeric>
#include <stdbool.h>


//bit operator
//&: and
//|: or
//^: xor
//~:invert

//0 & 0 = 0
//0 & 1 = 0
//1 & 0 = 0
//1 & 1 = 1

//0 | 0 = 0
//0 | 1 = 1
//1 | 0 = 1
//1 | 1 = 1

//0 ^ 0 = 0
//0 ^ 1 = 1
//1 ^ 0 = 1
//1 ^ 1 = 0

//0 ~  = 1
//1 ~  = 0

/*
 * Consider a number N and you need to find if N is a power of 2.
 * Simple solution to this problem is to repeated divide N by 2 if N is even.
 * If we end up with a 1 then N is power of 2, otherwise not.
 * There are a special case also. If N = 0 then it is not a power of 2
 */
bool isPowerOfTwo(int x)
{
    // x will check if x == 0 and !(x & (x - 1)) will check if x is a power of 2 or not
    return (x && !(x & (x - 1)));
}

/*
 *  Count the number of ones in the binary representation of the given number.
 */
int count_one (int n)
{
    int count = 0;
    while( n )
    {
        n = n&(n-1);
        count++;
    }
    return count;
}

// Check if the ith bit is set in the binary form of the given number.
bool check (int N, int i)
{
    if( N & (1 << i) )   // 1 = 0001
        return true;     // 1 << 2 = 0100
    else
        return false;
}

//Find the largest power of 2 (most significant bit in binary form),
//which is less than or equal to the given number N.
long largest_power(long N)
{
    //changing all right side bits to 1.
    N = N| (N>>1);
    N = N| (N>>2);
    N = N| (N>>4);
    N = N| (N>>8);


    //2.as now the number is 2 * x-1, where x is required answer, so adding 1 and dividing it by

    return (N+1)>>1;

}


/*
 * Nope, this should work. Note that if you take a ^ a, you will always get 0. Furthermore,
 * f you take a ^ x ^ a, you will always get x. Therefore, all the pairs of numbers will
 *  cancel out. For example, if you have n = 9 and A = {4, 9, 95, 93, 57, 4, 57, 93, 9},
 *  then you will effectively by computing the value of 4 ^ 9 ^ 95 ^ 93 ^ 57 ^ 4 ^ 57 ^ 93 ^ 9.
 *  It doesn't matter what order you take the xors, so this is equivalent to (4 ^ 4) ^ (9 ^ 9) ^ (95)
 *  ^ (93 ^ 93) ^ (57 ^ 57), which is 0 ^ 0 ^ 95 ^ 0 ^ 0, or just 95.
 */

int lonelyinteger() {

    int myInts[]={1,2,1,2,3};
    std::vector<int> a(myInts, myInts + sizeof(myInts) / sizeof(int)); //test case

    int result = std::accumulate(a.begin(), a.end(), 0, [](int k, int i){ return k = k ^ i;});
}

// Given two integers, L and R, find the maximal value of A xor B,
// where A and B satisfy the following condition:
// L <= A <= B <= R
int maxXor(int a, int b) {
    int value = a ^ b, result = 1;
    while (value) {
        value = value >> 1;
        result = result << 1;
    }
    return result - 1;
}

/* Function to get number of set bits in binary
   representation of passed binary no. */
unsigned int countSetBits(unsigned int n)
{
  unsigned int count = 0;
  while(n)
  {
    count += n & 1;
    n >>= 1;
  }
  return count;
}



int main()
{



    return 0;
}
