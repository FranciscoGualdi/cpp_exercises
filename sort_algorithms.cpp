
/*
 * some types of sort algorithms
 *
 */


#include <stdio.h>
#include <string.h>
#include <stdbool.h>


/*
 * C Program to Implement Selection Sort Recursively
 */
void selection(int list[], int i, int j, int size, int flag)
{
    int temp;

    if (i < size - 1)
    {
        if (flag)
        {
            j = i + 1;
        }
        if (j < size)
        {
            if (list[i] > list[j])
            {
                temp = list[i];
                list[i] = list[j];
                list[j] = temp;
            }
            selection(list, i, j + 1, size, 0);
        }
        selection(list, i + 1, 0, size, 1);
    }
}

//------------------------------------------------------------------------------------------------------

// Perform Quick Sort on a set of Entries from a File using Recursion (asc)
void quicksort(int list[], int low, int high)
{
    int pivot, i, j, temp;
    if (low < high)
    {
        pivot = low;
        i = low;
        j = high;
        while (i < j)
        {
            while (list[i] <= list[pivot] && i <= high)
            {
                i++;
            }
            while (list[j] > list[pivot] && j >= low)
            {
                j--;
            }
            if (i < j)
            {
                temp = list[i];
                list[i] = list[j];
                list[j] = temp;
            }
        }
        temp = list[j];
        list[j] = list[pivot];
        list[pivot] = temp;
        quicksort(list, low, j - 1);
        quicksort(list, j + 1, high);
    }
}




//----------------------------------------------------------------------------------------------------------

int main()
{

    int list[30] = {25,45,17,48,36,98,4,24,3,128,65,5,4,1};
    int size, temp, i, j;
    size = 14;

//    selection(list, 0, 0, size, 1);

    printf("[selection] The sorted list in ascending order is\n");
    for (i = 0; i < size; i++)
    {
        printf("%d  ", list[i]);
    }


    int list_1[30] = {25,45,17,48,36,98,4,24,3,128,65,5,4,1};

    quicksort(list_1,0,size-1); // can define a range of elements

    printf("[quicksort] The sorted list in ascending order is\n");
    for (i = 0; i < size; i++)
    {
        printf("%d  ", list_1[i]);
    }

    return 0;
}
