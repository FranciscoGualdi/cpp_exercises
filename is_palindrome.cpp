#include <iostream>
#include <list>

/*

Write a program that accepts 4 words as input (one per line) and identifies whether they are palindromes. A palindrome is a
word that reads the same when read back to front. The program should print the alphabetically sorted list of palindromes
(one per line). The program output should be in lowercase.

*/

#define MAXSIZE 4

int main()
{
    std::cout << "Init test_10" << std::endl;

    std::list<std::string> words;

    for (int var = 0; var < MAXSIZE ; ++var) {
        std::cout << "Enter a string: " << std::endl;
        char str[256];
        std::cin.getline(str,256);
        words.push_back(str);
    }

    // verify if palidrome
    std::list<std::string>::iterator it;
    for (it=words.begin(); it!=words.end(); ++it) {
        std::string s = *it;
        if( std::equal(s.begin(), s.begin() + s.size()/2, s.rbegin()) ) // just compare the first half with the second half in reverse
            std::cout << s << std::endl;
    }

    std::cout << "Exit test_10" << std::endl;
}
