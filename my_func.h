#ifndef MY_FUNC_H
#define MY_FUNC_H

#include <assert.h>

long toInt(char *s)
{
    char *p;
    long converted = strtol(s, &p, 10);
    assert(*p == 0 && "Failed to convert string in integer.");
    return converted;
}


#endif // MY_FUNC_H
