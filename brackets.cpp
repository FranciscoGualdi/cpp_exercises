// Verify consitency of brackets
#include <stdio.h>
#include <string.h>
#include <map>
#include <vector>
#include <stdbool.h>
 

bool brackets(std::string s)
{
    std::map<char,char> ref;
    ref['('] = ')';
    ref['{'] = '}';
    ref['['] = ']';

    std::vector<char> ref2;
    int l = s.length();
    if(l == 0) return true;

    for (int var = 0; var < l; ++var) {
        char k = s.at(var);
        auto res = ref.find(k);
        if(res != ref.end()){
            ref2.push_back(k);
        }
        else if(var != 0) {
            int index = ref2.size()-1;
            char last = 0;
            char comV = 0;
            if(index >= 0){
                last = ref2.at(index);
                comV = ref.at(last);
                if(comV == k){
                    ref2.erase(ref2.begin() + index);
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }
    return (ref2.size() == 0)?true:false;


}

int main()
{
    char str[] = {"{[()]}"}; // true
    bool res = brackets(str);

    char str1[] = {"{}[]()[]{}"}; //true
    res = brackets(str1);

    char str2[] = {"{[}]({)}"}; //false
    res = brackets(str2);

    return 0;
}
