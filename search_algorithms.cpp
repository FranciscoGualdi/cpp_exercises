
/*
 * some types of search algorithms
 *
 */


#include <stdio.h>
#include <string.h>
#include <stdbool.h>


// Binary search -> array must be sorted
int find_binary_search(int search, int array[], int n)
{

    int c, first, last, middle;

    first = 0;
    last = n - 1;
    middle = (first+last)/2;

    while (first <= last) {
        if (array[middle] < search)
            first = middle + 1;
        else if (array[middle] == search) {
            printf("%d found at location %d.\n", search, middle+1);
            return middle+1;
        }
        else
            last = middle - 1;

        middle = (first + last)/2;
    }
    if (first > last)
        printf("Not found! %d is not present in the list.\n", search);
    return -1;
}

int main()
{
    int search = 10; // element to search position 11
    int array[] = {25,45,17,48,36,98,4,24,3,128,10,65,5,4,1};
    int size = 14;
    int position = find_binary_search(search, array, size);

    return 0;
}
