

/*
 *  Program to find maximum overlap in a list of range integers


*/

#include <iostream>
#include <algorithm>
#include <string>
using namespace std;
#define MAXSIZE 100

int findMaxOverlap(int arrl[], int exit[], int n)
{
    sort(arrl, arrl+n);
    sort(exit, exit+n);

    int overlaps_in = 1, max_overlap = 1, time = arrl[0];
    int i = 1, j = 0;

    while (i < n && j < n)
    {
         // count +1
        if (arrl[i] <= exit[j])
        {
            overlaps_in++;

            if (overlaps_in > max_overlap)
            {
                max_overlap = overlaps_in;
                time = arrl[i];
            }
            i++;
        }
        else
        {
            overlaps_in--;
            j++;
        }
    }
    return max_overlap;
}

int main()
{
    int arrl[MAXSIZE];
    int exit[MAXSIZE];
    int number_customer_service = 0;
    int number_data_to_read = 0;

    //input
    std::cout << "Numbers of customer service: " << std::endl;
    char str[256];
    std::cin.getline(str,256); //size
    number_customer_service = atoi(str);

    std::cout << "Numbers line to read: " << std::endl;
    std::cin.getline(str,256); //size
    number_data_to_read = atoi(str);

    for (int var = 0; var < number_data_to_read && var < MAXSIZE ; ++var) {
        std::cout << "Enter the pair of timestamps separate by white space: " << std::endl;
        char temp[256];
        std::cin.getline(temp,256); //1001-80
        std::string str = temp;
        std::string start = str.substr(0, str.find(" "));
        std::string end = str.substr(str.find(" ")+1, str.size()-1);
        arrl[var] = atoi(start.c_str());
        exit[var] = atoi(end.c_str());
    }

    int max_overlap = findMaxOverlap(arrl, exit, number_data_to_read);

    if(max_overlap > number_customer_service)
        cout << max_overlap - number_customer_service << endl;
    else
        cout << "0" << endl;
    return 0;
}
