#include <iostream>
#include <vector>

/*

Write a program to print the hexadecimal values, octal values and the corresponding character, separated by hyphens, for
the ascii values between 40 and 126 (both inclusive).
The program should print the output in the following format with one result per line:
(hexadecimal value in uppercase)-(octal value. For two digit values the octal value should be preceded by '0')-
(corresponding character)

Ex. output

28-O50-(

7E-176-~

*/


int main()
{
    std::cout << "Init test_4" << std::endl;

    int element = 40;
    while( element <= 126)
    {
        printf("%02X-%03o-%c\n",element,element,element);
        element++;
    }

    std::cout << "Exit test_4" << std::endl;
}
