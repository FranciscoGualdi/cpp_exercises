#include <iostream>
#include <vector>
#include <algorithm>


/*

Write a program which will accept 4 inputted <key> <value> pairs. The program should sort the <key> <value> pairs in
ascending order of <key> and then print the <value> of each <key>. The printed <value> elements should be separated by a
space.


INPUT
6 a
1 This
2 is
9 test

Output

This is a test

*/


#define MaxSize 4

int main()
{
    std::cout << "Init test_8" << std::endl;

    std::vector<std::pair<std::string, std::string> > pairs;

    // INPUT
    for (int var = 0; var < MaxSize; ++var)
    {
        std::cout << "Enter <key> <word>:" << std::endl;

        char temp[256];
        std::cin.getline(temp,256); //1001-80
        std::string str = temp;
        std::string key = str.substr(0, str.find(" "));
        std::string value = str.substr(str.find(" ")+1, str.size()-1);
        pairs.push_back(std::pair<std::string, std::string>(key,value));
    }

    // order
    sort(pairs.begin(), pairs.end());

    //OUTPUT
    for (int var = 0; var < MaxSize; ++var)
    {
        std::cout << pairs[var].second << " ";
    }
    std::cout << "Exit test_8" << std::endl;
}
