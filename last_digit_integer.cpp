#include <iostream>
#include <vector>

/*

Write a program that prints the numbers between 258 and 393 (both inclusive) which do not end with 5. The program should
print the output so as to have one value per line. The output would therefore follow the below format:


Example output

value1
value2
value3


*/


int main()
{
    std::cout << "Init test_9" << std::endl;

    for (int var = 258; var <= 393; ++var) {
        int lastDigit = var % 10;

        if(lastDigit == 5)
            std::cout << var << " terminated with 5." << std::endl;

    }


    std::cout << "Exit test_9" << std::endl;
}
