#include <iostream>
#include <list>

/*
Write a program that accepts 10 student records (roll number and score) and prints them in decreasing order of scores. In
case there are multiple records pertaining to the same student, the program should choose a single record containing the
highest score. The program should be capable of accepting a multi-line input. Each subsequent line of input will contain a
student record, that is, a roll number and a score (separated by a hyphen). The output should consist of the combination of
roll number and corresponding score in decreasing order of score.

Input

1001-40
1002-50
1003-60
1002-80
1005-35
1007-68
1009-99
1009-10
1004-89

*/

#define MaxSize 10

struct Student {
    int roll;
    int score;
};

// to order
int compare(const Student& s1, const Student& s2)
{
    return s1.score > s2.score;
}

Student* getStudent(std::list<Student> &mylist,Student s)
{
    for (std::list<Student>::iterator it=mylist.begin(); it != mylist.end(); ++it)
        if((*it).roll == s.roll)
            return &(*it);

    return NULL;
}

int main()
{
    std::cout << "Init test_5" << std::endl;

    std::list<Student> students;

    // INPUT
    for (int var = 0; var < MaxSize; ++var)
    {
        std::cout << "Enter the student info formatted roll-score:" << std::endl;

        char temp[256];
        std::cin.getline(temp,256); //1001-80
        std::string str = temp;
        if(str.length() == 7) {
            Student s;
            s.roll = atoi(str.substr(0,4).c_str());
            s.score = atoi(str.substr(5,6).c_str());


            if(Student *myS = getStudent(students, s)) { // found student in the list
                if(s.score > myS->score)
                    myS->score = s.score;                // set new socre
            }
            else
                students.push_back(s);

        }
        else
            std::cout << "WRONG format!" << std::endl;
    }

    //order
    students.sort(compare);

    //OUTPUT
    std::cout << "OUTPUT:" << std::endl;

    for (std::list<Student>::iterator it=students.begin(); it != students.end(); ++it)
    {
        std::cout << (*it).roll << "-" << (*it).score << std::endl;
    }

    std::cout << "Exit test_5" << std::endl;
}
