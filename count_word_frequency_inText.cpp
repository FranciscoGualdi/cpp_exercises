#include <iostream>
#include <map>
#include <set>
#include <sstream>
/*
 * Counting words in a text
 */
class F {
    public:
        bool operator()(std::pair<int, std::string> lhs, std::pair<int, std::string> rhs) {
            if (lhs.first > rhs.first) return true;
            if (lhs.first == rhs.first && lhs.second < rhs.second) return true;
            return false;
        }
};
//split only white spaces
void split_whitespaces_delimiter(std::map<std::string, int> &m, std::string str)
{
    std::stringstream ss(str);
    std::string buf;
    while (ss >> buf)
        ++m[buf];;
}
int main() {
    std::map<std::string, int> m;
    std::string s = "amor love sexo love lamorrr sexxooo sexooo";
    split_whitespaces_delimiter(m,s);
//    while (std::cin >> s) {
//        ++m[s];
//    }
    std::multiset<std::pair<int, std::string>, F> mm;
    for (auto p: m) mm.insert(std::pair<int, std::string>{p.second, p.first});
    for (auto p: mm) {
        std::cout << p.second << "\t" << p.first << std::endl;
    }
}
