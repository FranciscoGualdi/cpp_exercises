
#include <stdio.h>
#include <string.h>
#include <map>
#include <vector>
#include <stdbool.h>
#include <math.h>
#include <unordered_map>


//-------------------------------------------------------
/*
 * Find the height of the tree
 */
struct Tree{
    int x;
    struct Tree *l;
    struct Tree *r;
};

int maxHeight(struct Tree *T) {
    int t1l = 0;
    int t2l = 0;

    if(T->l){
        t1l = 1 + maxHeight(T->l);
    }
    if(T->r){
        t2l = 1 + maxHeight(T->r);
    }

    return std::max(t1l,t2l);
}
// Returns minimum value in a given Binary Tree
int findMinValue(struct Tree *root)
{
    if(root == NULL)
        {return 0;}
    int res = root->x;
    int lres = findMinValue(root->l);
    int rres = findMinValue(root->r);
    //res = Math.min(res, Math.min(lres,rres));
    if (lres < res && lres != 0)
    res = lres;
    if (rres < res && rres != 0)
    res = rres;
    return res;
}
//------------------------------------------------------

// Returns maximum value in a given Binary Tree
int findMaxValue(struct Tree* root)
{
    // Base case
    if (root == NULL)
      return -1;

    // Return maximum of 3 values:
    // 1) Root's data 2) Max in Left Subtree
    // 3) Max in right subtree
    int res = root->x;
    int lres = findMaxValue(root->l);
    int rres = findMaxValue(root->r);
    if (lres > res)
      res = lres;
    if (rres > res)
      res = rres;
    return res;
}
//---------------------------------------
//find the amplitude of the tree
static int ans =0;
void helper(struct Tree* root, int min_val, int max_val) {
    if (!root) return;
    min_val = std::min(min_val, root->x);
    max_val = std::max(max_val, root->x);
    ans = std::max(ans, max_val - min_val);
    helper(root->l, min_val, max_val);
    helper(root->r, min_val, max_val);
}
int maxAmp(struct Tree* root) {
    if (root == NULL) return 0;
    helper(root, root->x, root->x);
    return ans;
}

// A utility function to create a new node
struct Tree* newNode(int data)
{
    struct Tree* node = (struct Tree*)
            malloc(sizeof(struct Tree));
    node->x = data;
    node->l = node->r = NULL;
    return(node);
}

// count the number of distict member in one the path
void countOfDistinctMembers(Tree* root, std::unordered_map<int,int>& hash,int &max){
    if (root==NULL){
        if (hash.size()>=max) max=hash.size();
        return;
    }

    hash[root->x]+=1;

    countOfDistinctMembers(root->l,hash,max);
    countOfDistinctMembers(root->r,hash,max);

    auto it = hash.find(root->x);
    if (it->second<=1)
    {
        hash.erase(it);
    }else{
        it->second--;
    }
}
//-----------------------------------
/*
 * In a binary tree, if in the path from root to the node A, there is no node with greater
 * value than A’s, this node A is visible. We need to count the number of visible nodes in a
 *  binary tree. The root node is count as visible node.

 */
int helper_countVisible(Tree *node, int ancesterMax) {
    if (node == NULL) {
        return 0;
    }
    int newMax = std::max(ancesterMax, node->x);
    if (node->x > ancesterMax) {
        return 1 + helper_countVisible(node->l, newMax) + helper_countVisible(node->r, newMax);
    } else {
        return helper_countVisible(node->l, newMax) + helper_countVisible(node->r, newMax);
    }
}

int countVisible(Tree *root) {
    int res =0;
    return helper_countVisible(root, res);
}
//-----------------------------------
/*
 * counting the number of nodes in a tree
 */
int count(Tree *n)
{
    int c = 1;

    if (n == NULL)
        return 0;
    else
    {
        c += count(n->l);
        c += count(n->r);
        return c;
     }
}
//--------------------------------------------

/*
 * Displaying the nodes of tree in preorder, can be util to put in a list/vector
 */
void preorder(Tree *t)
{
    if (t != NULL)
    {
        printf("%d->", t->x);
        preorder(t->l);
        preorder(t->r);
    }
 }

//--------------------------------
int main()
{
    struct Tree* NewRoot=NULL;
    struct Tree *root = newNode(2);
    root->l        = newNode(7);
    root->r       = newNode(5);
    root->l->r = newNode(6);
    root->l->r->l = newNode(1);
    root->l->r->r = newNode(11);
    root->r->r = newNode(9);
    root->r->r->l = newNode(4);

    printf ("Numbers of node is %d \n", count(root));
    printf ("Maximum amplitude is %d \n", maxAmp(root));
    printf ("Maximum height is %d \n", maxHeight(root));
    printf ("Minimum Value is %d \n", findMinValue(root));
    printf ("Maximum Value is %d \n", findMaxValue(root));


    std::unordered_map<int,int> hash;
    int max=0;
    countOfDistinctMembers(root,hash,max);
    printf ("countOfDistinctMembers is %d \n", max);

    printf ("countVisible is %d \n", countVisible(root));


    return 0;
}


//private static int Solution(Tree T)
//        {
//            if (T == null)
//            {
//                return -1;
//            }

//            var count = 0;

//            var stack = new Stack<Tuple<int, Tree>>();
//            stack.Push(new Tuple<int, Tree>(T.x, T));

//            while (stack.Count > 0)
//            {
//                var n = stack.Pop();
//                if (n.Item1 <= n.Item2.x)
//                {
//                    ++count;
//                }
//                var max = Math.Max(n.Item1, n.Item2.x);
//                if (n.Item2.l != null)
//                {
//                    stack.Push(new Tuple<int, Tree>(max, n.Item2.l));
//                }
//                if (n.Item2.r != null)
//                {
//                    stack.Push(new Tuple<int, Tree>(max, n.Item2.r));
//                }
//            }

//            return count;
//        }
