#include <stdio.h>
#include <sstream>
#include <iostream>
#include <list>
#include "my_func.h"

/*
Write a program which will accept two lists comprising of different numbers. The program should merge the two lists, sort
them, and print them in reverse order.
Below is an example of the input supplied to the program. The first line of input signifies the size of the first list, the second
line signifies the size of the second list, followed by sets of different numbers (one set per line).
Input
-----
2 (Signifying the size of first list)
4 (Signifying the size of second list)
45 56
67 88 90 1
Output
------
90
88
67
56
45
1
*/

// split string in a list of string
void split(std::list<std::string> &mylist, std::string str, char delimiter, int size)
{
    int cont = 0;
    std::istringstream f(str);
    std::string s;
    while (getline(f, s, delimiter) && cont < size) {
        mylist.push_back(s);
        cont++;
    }
}

//split only white spaces
void split_whitespaces_delimiter(std::list<std::string> &mylist, std::string str)
{
    std::stringstream ss(str);
    std::string buf;
    while (ss >> buf)
        mylist.push_back(buf);
}

int main()
{
    // split a std::string in a std::list and count how many elements to split
    std::list<std::string> elements;
    int max_to_split = 3;
    std::string str = "A B CD E F GH I JH";
    char delimiter = ' ';
    // parse list 1
    split(elements, str, delimiter, max_to_split);

   std::list<std::string> elements2;
    split_whitespaces_delimiter(elements2,str);

    //OUTPUT=
    std::cout << "All elements:" << std::endl;

    for (std::list<std::string>::iterator it = elements.begin(); it != elements.end(); it++)
        std::cout << *it << std::endl;

    std::cout << "All elements2(only white spaces):" << std::endl;

    for (std::list<std::string>::iterator it = elements2.begin(); it != elements2.end(); it++)
        std::cout << *it << std::endl;

    return 0;
}
