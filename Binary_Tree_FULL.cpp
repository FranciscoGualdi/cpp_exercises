#include <iostream>
#include <stdio.h>
#include <string.h>
#include <map>
#include <vector>
#include <list>
#include <stdbool.h>
#include <math.h>
#include <unordered_map>

struct node
{
    int key;
    struct node *left, *right;
};


// find a key in the tree, tree must be sort
struct node* search(struct node* root, int key)
{
    // Base Cases: root is null or key is present at root
    if (root == NULL || root->key == key)
        return root;
    
    // Key is greater than root's key
    if (root->key < key)
        return search(root->right, key);

    // Key is smaller than root's key
    return search(root->left, key);
}

// A utility function to create a new BST node
struct node *newNode(int item)
{
    struct node *temp =  (struct node *)malloc(sizeof(struct node));
    temp->key = item;
    temp->left = temp->right = NULL;
    return temp;
}

// A utility function to do inorder traversal of BST
void inorder(struct node *root)
{
    if (root != NULL)
    {
        inorder(root->left);
        printf("%d \n", root->key);
        inorder(root->right);
    }
}

// Displaying the nodes of tree in preorder, can be util to put in a list/vector
void preorder(struct node *t)
{
    if (t != NULL)
    {
        printf("%d->", t->key);
        preorder(t->left);
        preorder(t->right);
    }
}

//In this traversal technique the traversal order is left-right-root.
void postorder(struct node *root)
{
    if(root)
    {
        postorder(root->left);    //Go to left sub tree
        postorder(root->right);     //Go to right sub tree
        printf("%d ",root->key);    //Printf root->data
    }
}

// level order traversal
void levelOrder(struct node* node)
{
    std::list<struct node*> q;

    q.push_back(node);
    while(!q.empty()){
        struct node * top = q.front();
        q.pop_back();
        if(top == NULL){
            continue;
        }

        printf("%d ",top->key);

        q.push_back(top->left);
        q.push_back(top->right);
    }
}

// A utility function to insert a new node with given key in BST
struct node* insert(struct node* node, int key)
{
    /* If the tree is empty, return a new node */
    if (node == NULL) return newNode(key);

    /* Otherwise, recur down the tree */
    if (key < node->key)
        node->left  = insert(node->left, key);
    else if (key > node->key)
        node->right = insert(node->right, key);

    /* return the (unchanged) node pointer */
    return node;
}

// Given a non-empty binary search tree, return the node with minimum
// key value found in that tree. Note that the entire tree does not
// need to be searched.
struct node * minValueNode(struct node* node)
{
    struct node* current = node;

    /* loop down to find the leftmost leaf */
    while (current->left != NULL)
        current = current->left;

    return current;
}

// Returns minimum value in a given Binary Tree
int minKeyValueNode(struct node *root)
{
    if(root == NULL)
    {return 0;}
    int res = root->key;
    int lres = minKeyValueNode(root->left);
    int rres = minKeyValueNode(root->right);
    //res = Math.min(res, Math.min(lres,rres));
    if (lres < res && lres != 0)
        res = lres;
    if (rres < res && rres != 0)
        res = rres;
    return res;
}
// Returns maximum value key in a given Binary Tree
int maxKeyValueNode(struct node* root)
{
    // Base case
    if (root == NULL)
        return -1;

    // Return maximum of 3 values:
    // 1) Root's data 2) Max in Left Subtree
    // 3) Max in right subtree
    int res = root->key;
    int lres = maxKeyValueNode(root->left);
    int rres = maxKeyValueNode(root->right);
    if (lres > res)
        res = lres;
    if (rres > res)
        res = rres;
    return res;
}

// Given a binary search tree and a key, this function deletes the key
// and returns the new root
struct node* deleteNode(struct node* root, int key)
{
    // base case
    if (root == NULL) return root;

    // If the key to be deleted is smaller than the root's key,
    // then it lies in left subtree
    if (key < root->key)
        root->left = deleteNode(root->left, key);

    // If the key to be deleted is greater than the root's key,
    // then it lies in right subtree
    else if (key > root->key)
        root->right = deleteNode(root->right, key);

    // if key is same as root's key, then This is the node
    // to be deleted
    else
    {
        // node with only one child or no child
        if (root->left == NULL)
        {
            struct node *temp = root->right;
            free(root);
            return temp;
        }
        else if (root->right == NULL)
        {
            struct node *temp = root->left;
            free(root);
            return temp;
        }

        // node with two children: Get the inorder successor (smallest
        // in the right subtree)
        struct node* temp = minValueNode(root->right);

        // Copy the inorder successor's content to this node
        root->key = temp->key;

        // Delete the inorder successor
        root->right = deleteNode(root->right, temp->key);
    }
    return root;
}

/* counting the number of nodes in a tree
*/
int count(struct node *n)
{
    int c = 1;

    if (n == NULL)
        return 0;
    else
    {
        c += count(n->left);
        c += count(n->right);
        return c;
    }
}

//find the height/amplitude of the tree
int height(struct node* root) {
    if(root == NULL){
        return -1;
    }
    int leftHeight = height(root->left) + 1;
    int rightHeight = height(root->right) + 1;
    return std::max(leftHeight , rightHeight);
}

// count the number of distict member in the three
void countOfDistinctMembers(struct node* root, std::unordered_map<int,int>& hash,int &max){
    if (root==NULL){
        if (hash.size()>=max) max=hash.size();
        return;
    }

    hash[root->key]+=1;

    countOfDistinctMembers(root->left,hash,max);
    countOfDistinctMembers(root->right,hash,max);

    auto it = hash.find(root->key);
    if (it->second<=1)
    {
        hash.erase(it);
    }else{
        it->second--;
    }
}

/*
The lowest common ancestor between two nodes n1 and n2 is defined as the lowest node in BST that has both n1 and n2 as descendants (where we allow a node to be a descendant of itself).
Consider 3 cases :

    Both key1 and key2 are on left side of a node.
    Both key1 and key2 are on right side of a node.
    One is on left side and other is on right side.
*/
struct node * lca(struct node * root, int v1,int v2){
    if(root->key > v1 && root->key > v2){ // check if both keys are to the left of curr. node
        return lca(root->left, v1, v2);
    }
    if(root->key < v1 && root->key < v2){ // check if both keys are to the right of curr. node
        return lca(root->right, v1, v2);
    }
    return root;                            // return the LCA
}

/*
 * In a binary tree, if in the path from root to the node A, there is no node with greater
 * value than A’s, this node A is visible. We need to count the number of visible nodes in a
 *  binary tree. The root node is count as visible node.

 */
int helper_countVisible(struct node *root, int ancesterMax) {
    if (root == NULL) {
        return 0;
    }
    int newMax = std::max(ancesterMax, root->key);
    if (root->key > ancesterMax) {
        return 1 + helper_countVisible(root->left, newMax) + helper_countVisible(root->right, newMax);
    } else {
        return helper_countVisible(root->left, newMax) + helper_countVisible(root->right, newMax);
    }
}
int countVisible(struct node *root) {
    int res =0;
    return helper_countVisible(root, res);
}

// Driver Program to test above functions
int main()
{
    /* Let us create following BST
              50
           /     \
          30      70
         /  \    /  \
       20   40  60   80 */
    struct node *root = NULL;
    root = insert(root, 50);
    insert(root, 30);
    insert(root, 20);
    insert(root, 40);
    insert(root, 70);
    insert(root, 60);
    insert(root, 80);

    // print inoder traversal of the BST
    std::cout << "inorder: " << std::endl;
    inorder(root);
    std::cout << std::endl;

    std::cout << "preorder: " << std::endl;
    preorder(root);
    std::cout << std::endl;

    std::cout << "postorder: " << std::endl;
    postorder(root);
    std::cout << std::endl;

    std::cout << "levelOrder: " << std::endl;
    levelOrder(root);
    std::cout << std::endl;

    std::cout << "count: " << count(root) << std::endl;
    std::cout << std::endl;

    std::cout << "height: " << height(root) << std::endl;
    std::cout << std::endl;

    std::cout << "minKeyValue: " << minKeyValueNode(root) << std::endl;
    std::cout << std::endl;

    std::cout << "maxKeyValue: " << maxKeyValueNode(root) << std::endl;
    std::cout << std::endl;

    return 0;
}
