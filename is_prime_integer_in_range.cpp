#include <iostream>
#include <vector>

/*

Write a program which takes a single integer as input and prints "TRUE" if the integer is a prime number between 1 and 500
(1 is to be considered as a prime number). If the number is not prime the program should print "FALSE". If the number
supplied is outside the range the program should print "OUT OF RANGE". If 0 is supplied as input, the program should stop
executing. The program should print all outputs in uppercase.


Input

1
2
3
4
678
5
0
78

*/

bool isPrime(int num) {
    int i;
    for (i=2; i*i<=num; i++) {
        if (num == 2) {
            printf("2 is prime!");
            return true;
        }

        else if (num < 0) {
            printf("Number has to be positive!");
            return false;
        }

        // Check if num has a perfect divisor between 1 and itself.

        else if (num % i == 0) {
            printf("%d is not prime!", num);
            return false;
        }
    }

}
//----------------------
bool isPrimeFaster(int n)
{
    // Corner cases
    if (n <= 1)  return false;
    if (n <= 3)  return true;

    // This is checked so that we can skip
    // middle five numbers in below loop
    if (n%2 == 0 || n%3 == 0) return false;

    for (int i=5; i*i<=n; i=i+6)
        if (n%i == 0 || n%(i+2) == 0)
           return false;

    return true;
}


int main()
{
    std::cout << "Init test_6" << std::endl;

    // INPUT
    while(true)
    {
        std::cout << "Enter a number:" << std::endl;
        char temp[10];
        std::cin.getline(temp,10);
        int number= atoi(temp);

        if(number == 0)
            break;

        if(number >= 1 && number <= 500)
        {
            if(isPrime(number)) // prime
                std::cout << "TRUE" << std::endl;
            else
                std::cout << "FALSE" << std::endl;
        }
        else
            std::cout << "OUT OF RANGE" << std::endl;


    }

    std::cout << "Exit test_6" << std::endl;
}
