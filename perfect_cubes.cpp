#include <iostream>
#include <vector>
#include "math.h"


/*
Write a program which prints the numbers between 500 and 10000 (both inclusive) that are perfect cubes. The program
should print one number per line. The number 8 is an example of a perfect cube as 2*2*2 = 8.
*/

int main()
{
    std::cout << "Init test_2" << std::endl;

    int max = 10000, lim,start;
    int min = 500;

    start = cbrt(min);
    lim = cbrt(max);

    std::cout << "Perfect cubes btw 500 and 10000: " << std::endl;
    for(int i = start+1; i <= lim; i++)
    {
        std::cout << i << " ^ 3  : " << pow(i, 3) << std::endl;
    }

    std::cout << "Exit test_2" << std::endl;
}
