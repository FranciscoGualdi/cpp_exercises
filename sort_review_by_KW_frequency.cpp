#include <iostream>
#include <unordered_map>
#include <map>
#include <stdio.h>
#include <string.h>
#include <string>
#include <algorithm>
#include <list>

using namespace std;

struct H_Review {
    int id;
    string review;
    map<string, int> key_words_frequency;
};

// Search the keyword inside the review and cont the number of occurrence
void search_keywords (list<H_Review> &h_reviews,
                      vector<string> &key_words)
{
    for (std::list<H_Review>::iterator it = h_reviews.begin(), end = h_reviews.end(); it != end; ++it)
    {
        const char *str = it->review.c_str();

        for (int var = 0; var < key_words.size(); ++var) {
            string what = key_words[var].c_str();

            int what_len = what.size();
            int count = 0;

            char *where = (char*)str;

            if (what_len)
                while ((where = strstr(where, what.c_str()))) {
                    where += what_len;
                    // if not exist insert else increment
                    it->key_words_frequency[what] = (it->key_words_frequency[what] + 1);
                }
        }
    }
}

// sort by number of occurence of the key word in the review
void order_by_keyword_frequency( list<H_Review> &h_reviews)
{
    h_reviews.sort([](const H_Review & a, const H_Review & b)
    {
        int total_occurrences_a = 0;
        for (const auto &pair : a.key_words_frequency) {
            total_occurrences_a += pair.second;
        }
        int total_occurrences_b = 0;
        for (const auto &pair : b.key_words_frequency) {
            total_occurrences_b += pair.second;
        }
        return total_occurrences_b < total_occurrences_a;
    }
    );
}

void print_reviews(list<H_Review> &h_reviews_ordered)
{
    for ( const auto it : h_reviews_ordered)
    {
        cout << "Review text: " << it.review << endl;
        for (const auto &pair : it.key_words_frequency) {
            cout << "       " << pair.first << ": " << pair.second << endl;
        }
        cout << endl;
    }
}

int main()
{
    list<H_Review> h_reviews;
    vector<string> key_words;

    // mock reviews
    H_Review h_review_1;
    h_review_1.review = "String1 subString1 Strinstrnd subStr ing1subString c++";
    h_reviews.push_back(h_review_1);

    H_Review h_review_2;
    h_review_2.review = "brazil France Netherlands Italy Spain";
    h_reviews.push_back(h_review_2);

    H_Review h_review_3;
    h_review_3.review = "C++ Java Python c# .NET PHP JSP CSS brazil";
    h_reviews.push_back(h_review_3);

    // mock key words
    key_words.push_back("String1");
    key_words.push_back("brazil");
    key_words.push_back("C++");
    key_words.push_back("Java");

    // search for the key words
    search_keywords( h_reviews, key_words);

    // sort by frequency of keywords
    order_by_keyword_frequency( h_reviews );

    // print sort
    print_reviews( h_reviews );

}
