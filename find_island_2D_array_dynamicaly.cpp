/*



*/

#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <iostream>


using namespace std;

int x[8]={1,0,-1,-1,-1,0,1,1};
int y[8]={-1,-1,-1,0,1,1,1,0};

static int count=0;

void print(int **a,int row,int col)
{
    for(int i=0;i<row;i++)
    {
        for(int j=0;j<col;j++)
            cout<<a[i][j]<<" ";
        cout<<endl;
    }
}


int isValid(int **a,int i,int j,int row,int col,int **visited)
{
    if(i<0 || j<0 || i>=row || i>=col || !a[i][j]|| visited[i][j])
        return 0;
    return 1;
}
int isSafe(int **a,int i,int j,int row,int col)
{
    if(i<0 || j<0 || i>=row || j>=col || a[i][j]==-1 || !a[i][j])
        return 0;
    return 1;
}
void getCountOfBunchOf1s(int **a,int i,int j,int row,int col,int **visited)
{
    visited[i][j]=1;
    for(int k=0;k<8;k++)
    {
        if(isValid(a,i+x[k],j+y[k],row,col,visited))
            getCountOfBunchOf1s(a,i+x[k],j+y[k],row,col,visited);
    }
}
int getConnected1s(int **a,int i,int j,int row,int col)
{
    a[i][j]=-1;

    for(int k=0;k<8;k++)
    {
        if(isSafe(a,i+x[k],j+y[k],row,col))
        {
            count++;
            getConnected1s(a,i+x[k],j+y[k],row,col);
        }
    }
    return count;
}

int main()
{
    // dynamicaly create de 2d array

    int **a,**visited,m,n,count1=0;
    cout<<"\nEnter no. of rows : ";
    cin>>n;
    cout<<"\nEnter no. of columns : ";
    cin>>m;
    cout<<"\nEnter "<<n*m<<" data : \n";

    a = (int**)malloc(sizeof(int*)*n);
    visited = (int**)malloc(sizeof(int*)*n);
    for(int i=0;i<n;i++)
    {
        a[i]=(int*)malloc(sizeof(int)*m);
        visited[i]=(int*)malloc(sizeof(int)*m);
    }

    //user enter data, one digit per line
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
        {
            cin>>a[i][j];
            visited[i][j]=0;
        }
    }

    // first method
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
        {
            if(a[i][j] && !visited[i][j])
            {
                getCountOfBunchOf1s(a,i,j,n,m,visited);	//DFS
                count1++;
            }
        }
    }

    cout<<"\nEntered 2D Array :\n";
    print(a,n,m);
    cout<<"\nNo. of connected 1's : "<<count1;
    count1=0;
    int size=0,maxsize=0;
    for(int i=0;i<n;i++)
    {
        for(int j=0;j<m;j++)
        {
            if(a[i][j]==1)
            {
                count++;
                size = getConnected1s(a,i,j,n,m);
                if(size>maxsize)
                    maxsize=size;
                count1++;
                count=0;
            }

        }
    }
    cout<<"\nNo. of connected 1's (other Method) : "<<count1<<endl;
    cout<<"\nSize of largest connected 1's : "<<maxsize<<endl;

    return 0;
}



