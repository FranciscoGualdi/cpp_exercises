#include <iostream>
#include <list>

/*
Write a program which will accept 6 words as input sort them alphabetically and then print them (one per line).


Input

Tennis
Polo
Apple
Basktball
Golf
Soccer

*/

#define MaxSize 6

int main()
{
    std::cout << "Init test_7" << std::endl;
    std::list<std::string> words;

    // INPUT
    for (int var = 0; var < MaxSize; ++var)
    {
        std::cout << "Enter one word:" << std::endl;

        char temp[256];
        std::cin.getline(temp,256); //1001-80
        std::string str = temp;
        words.push_back(str);
    }

    //order
    words.sort();


    std::list<std::string>::iterator it;
    for (it=words.begin(); it!=words.end(); ++it)
        std::cout << *it <<std::endl;

    std::cout << "Exit test_7" << std::endl;
}
