import java.util.ArrayList;
import java.util.Collections;

public class Solution {

	public int ps(int[] A) {
		ArrayList<Integer> unique = new ArrayList<Integer>();
		int result = A.length;
		for (int i = 0; i < A.length; i++) {
			int val = A[i];
			int pos = Collections.binarySearch(unique, val);
			if (pos < 0) {
				// Not found
				result =i;
				unique.add(-1 * pos - 1, val);
			} else {
				// Found do nothing
			}
		}
		return result;
	}
}