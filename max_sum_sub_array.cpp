/*

sub array is consecutives elements (or just one) inside a array

[1, -3, 2, 1, -1]

-3,2 is a subarray
2,1 is a subarray
1, is a subarray

the max sum is 2,1 = 3

*/

#include <iostream>
#include <cstdlib>


int max_sum_subarray(int arr[], int size)
{
    int max_current;
    int max_global;
    max_current = max_global = arr[0];

    for (int var = 1; var < size-1; ++var) {
        max_current = (arr[var] > (max_current + arr[var]))?arr[var]:(max_current + arr[var]);
        if(max_current > max_global)
            max_global = max_current;
    }
    return max_global;
}

int main()
{
    int arr[]={1, -3, 2, 1, -1};

    int res = max_sum_subarray(arr,5);

    std::cout << res << std::endl;

    return 0;
}
